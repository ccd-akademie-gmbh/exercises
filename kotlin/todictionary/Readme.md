# Kata: ToDictionary

## DE:
Die implementierte Methode ToDictionary konvertiert einen String in ein Dictionary( in Kotlin in eine Map).

Der String enthält Key/Value-Paare, die durch ein Semikolon getrennt sind, beispielsweise:

    “a=1;b=2;c=3” 

Die Methode konvertiert diesen String in ein Dictionary(Map) mit den folgenden Key/Value-Paaren:

    val mutableMap = mutableMapOf("a" to "1", "b" to "2", "c" to "3")

Das ist nur ein Beispiel. Die Methode implementiert weitere Konvertierungseigenschaften.

### Aufgabe: 
* Lege ein Testprojekt an
* Implementiere Testfälle für die Methode ToDictionary

### Hinweis:
Sollte es nicht möglich sein, das Repository zu klonen, dann kopiere den Code aus dem Browser in ein leeres Kotlin Projekt.

## EN:
The implemented method ToDictionary converts a string into a dictionary (in Kotlin into a map).

The string contains key/value pairs separated by a semicolon, for example:

    “a=1;b=2;c=3” 

The method converts this string into a dictionary(map) with the following key/value pairs:

    val mutableMap = mutableMapOf(“a” to “1”, “b” to “2”, “c” to “3”)

This is just an example. The method implements further conversion properties.

### Exercise:
* Create a test project
* Implement test cases for the ToDictionary method

### Note:
If it is not possible to clone the repository, copy the code from the browser into an empty Kotlin project.

Translated with DeepL.com (free version)