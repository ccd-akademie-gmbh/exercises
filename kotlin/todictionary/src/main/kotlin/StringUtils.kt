package org.example

class StringUtils {
    fun ToDictionary(input: String): Map<String, String> {
        val settings = splitIntoSettings(input)
        val keyValuePairs = splitIntoKeyAndValue(settings)
        val dictionary = createDictionary(keyValuePairs)
        
        return dictionary
    }

    fun splitIntoSettings(input: String): List<String> {
        return input.split(";")
    }

    fun splitIntoKeyAndValue(settings: List<String>): List<Pair<String, String>> {
        return settings.map { setting ->
            val keyValue = setting.split("=")
            if(keyValue[0].isEmpty()) {
                throw Exception("Key must not be empty")
            }
            Pair(keyValue[0], keyValue[1])
        }
    }

    fun createDictionary(keyValuePairs: List<Pair<String, String>>): Map<String, String> {
        val dictionary = mutableMapOf<String, String>()
        for(pair in keyValuePairs) {
            dictionary[pair.first] = pair.second
        }
        return dictionary
    }
}