package org.example

enum class Command {
    None,
    Add,
    List,
    Done,
    Remove,
    Quit
}
