package org.example

class ConsoleUi(private val todoList: TodoList) {

    fun run() {
        var command: Command
        do {
            command = getCommand()
            when (command) {
                Command.Add -> todoList.add()
                Command.List -> todoList.list()
                Command.Done -> todoList.done()
                Command.Remove -> todoList.remove()
                else -> {}
            }
        } while (command != Command.Quit)
    }

    private fun getCommand(): Command {
        var command: Command
        do {
            println()
            println("A)dd L)ist D)one R)emove Q)uit")
            print("Enter a command: ")
            val input = readLine()
            command = commandFromInput(input)
        } while (command == Command.None)
        return command
    }

    private fun commandFromInput(input: String?): Command {
        if (input.isNullOrBlank()) {
            return Command.None
        }

        return when (input.lowercase()) {
            "a" -> Command.Add
            "l" -> Command.List
            "d" -> Command.Done
            "r" -> Command.Remove
            "q" -> Command.Quit
            else -> Command.None
        }
    }

    fun promptForATask() {
        print("Enter a task: ")
    }

    fun readText(): String? {
        return readLine()
    }

    fun listTasks(tasks: List<Todo>) {
        println()
        println("Tasks:")
        tasks.forEachIndexed { index, task ->
            println("${index + 1}: ${if (task.done) "X " else "  "}${task.text}")
        }
    }

    fun promptForATaskNumber() {
        println()
        print("Enter the number of a task: ")
    }

    fun readTaskNumber(): String? {
        return readLine()
    }
}
