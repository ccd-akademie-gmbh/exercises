package org.example

class TodoList(private val todoStore: TodoStore) {
    private lateinit var consoleUi: ConsoleUi
    private val tasks: MutableList<Todo> = todoStore.load()

    fun setConsoleUi(consoleUi: ConsoleUi) {
        this.consoleUi = consoleUi
    }

    fun add() {
        consoleUi.promptForATask()
        val text = consoleUi.readText()
        if (!text.isNullOrBlank()) {
            tasks.add(Todo(text, false))
            todoStore.save(tasks)
        }
    }

    fun list() {
        consoleUi.listTasks(tasks)
    }

    fun done() {
        consoleUi.listTasks(tasks)
        consoleUi.promptForATaskNumber()
        val input = consoleUi.readTaskNumber()
        val index = input!!.toIntOrNull()
        if (index != null && index > 0 && index <= tasks.size) {
            tasks[index - 1].done = true
            todoStore.save(tasks)
        }
    }

    fun remove() {
        consoleUi.listTasks(tasks)
        consoleUi.promptForATaskNumber()
        val input = consoleUi.readTaskNumber()
        val index = input!!.toIntOrNull()
        if (index != null && index > 0 && index <= tasks.size) {
            tasks.removeAt(index - 1)
            todoStore.save(tasks)
        }
    }
}
