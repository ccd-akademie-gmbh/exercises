class Car(
    private val fuelSystem: FuelSystem,
    private val engine: Engine,
    private val diagnostics: Diagnostics
) {

    fun startCar(): Boolean {
        return engine.start()
    }

    fun stopCar() {
        engine.stop()
    }

    fun checkCarStatus(): String {
        return diagnostics.runDiagnostics()
    }

    fun refuelCar(amount: Int) {
        fuelSystem.refuel(amount)
    }

    fun getFuelLevel(): Int {
        return fuelSystem.getFuelLevel()
    }
}
