package ccd

object Shorty {
    @JvmStatic
    fun shorten(s: String, length: Int): String {
        if (s.length <= length) {
            return s
        }

        if (length < 7) {
            return s.substring(0, length)
        }

        val sampleLength = (length - 3) / 2
        var extraLength = 0
        if (sampleLength * 2 + 3 < length) {
            extraLength = 1
        }

        val prefix = s.substring(0, sampleLength + extraLength)
        val suffix = s.substring(s.length - sampleLength)
        return "$prefix...$suffix"
    }
}