using System;
using System.Collections.Generic;
using System.IO;

public static class FileHelper
{
    public static void AppendAllLines(string filePath, List<string> lines)
    {
        File.AppendAllLines(filePath, lines);
    }

    public static List<string> ReadAllLines(string filePath)
    {
        return new List<string>(File.ReadAllLines(filePath));
    }
}