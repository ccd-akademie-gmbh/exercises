using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        BusinessLogic logic = new BusinessLogic();
        while (true)
        {
            Console.Write("Name: ");
            string name = Console.ReadLine();
            FileHelper.AppendAllLines("guestlist.txt", new List<string> { name });
            List<string> names = FileHelper.ReadAllLines("guestlist.txt");
            Dictionary<string, int> visits = new Dictionary<string, int>();

            foreach (var n in names)
            {
                if (!visits.ContainsKey(n))
                    visits[n] = 1;
                else
                    visits[n] += 1;
            }

            string greeting = logic.DetermineGreeting(name, visits);
            Console.WriteLine(greeting);
        }
    }
}