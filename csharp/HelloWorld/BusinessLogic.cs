using System;
using System.Collections.Generic;

public class BusinessLogic
{
    public string DetermineGreeting(string name, Dictionary<string, int> visits)
    {
        Templates templates = new Templates();
        if (visits.TryGetValue(name, out int visitCount))
        {
            string result = templates.Get(visitCount);
            return result + name + "!";
        }
        return "Hello " + name + "!";
    }
}