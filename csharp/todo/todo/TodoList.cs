namespace todo;

public class TodoList
{
    private ConsoleUi _consoleUi;
    private readonly TodoStore _todoStore;
    private readonly List<Todo> _tasks;

    public TodoList(TodoStore todoStore) {
        _todoStore = todoStore;
        _tasks = todoStore.Load();
    }
    
    public void SetConsoleUi(ConsoleUi consoleUi) {
        _consoleUi = consoleUi;
    }

    public void Add() {
        _consoleUi.PromptForAsTask();
        var text = _consoleUi.ReadText();
        if (!string.IsNullOrWhiteSpace(text)) {
            _tasks.Add(new Todo(text));
            _todoStore.Save(_tasks);
        }
    }

    public void List() {
        _consoleUi.ListTasks(_tasks);
    }

    public void Done() {
        _consoleUi.ListTasks(_tasks);
        _consoleUi.PromptForATaskNumber();
        var input = _consoleUi.ReadTaskNumber();
        if (int.TryParse(input, out var index) && index > 0 && index <= _tasks.Count) {
            _tasks[index - 1].Done = true;
            _todoStore.Save(_tasks);
        }
    }

    public void Remove() {
        _consoleUi.ListTasks(_tasks);
        _consoleUi.PromptForATaskNumber();
        var input = _consoleUi.ReadTaskNumber();
        if (int.TryParse(input, out var index) && index > 0 && index <= _tasks.Count) {
            _tasks.RemoveAt(index - 1);
            _todoStore.Save(_tasks);
        }
    }
}