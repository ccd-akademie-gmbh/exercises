namespace todo;

public class ConsoleUi
{
    private readonly TodoList _todoList;

    public ConsoleUi(TodoList todoList) {
        _todoList = todoList;
    }

    public void Run() {
        Command command;
        do {
            command = GetCommand();
            switch (command) {
                case Command.Add:
                    _todoList.Add();
                    break;
                case Command.List:
                    _todoList.List();
                    break;
                case Command.Done:
                    _todoList.Done();
                    break;
                case Command.Remove:
                    _todoList.Remove();
                    break;
            }
        } while (command != Command.Quit);
    }

    private Command GetCommand() {
        Command command;
        do {
            Console.WriteLine();
            Console.WriteLine("A)dd L)ist D)one R)emove Q)uit");
            Console.Write("Enter a command: ");
            var input = Console.ReadLine();
            command = CommandFromInput(input);
        } while (command == Command.None);
        return command;
    }

    private Command CommandFromInput(string? input) {
        if (string.IsNullOrWhiteSpace(input)) {
            return Command.None;
        }

        return input switch {
            "a" or "A" => Command.Add,
            "l" or "L" => Command.List,
            "d" or "D" => Command.Done,
            "r" or "R" => Command.Remove,
            "q" or "Q" => Command.Quit,
            _ => Command.None
        };
    }

    public void PromptForAsTask() {
        Console.Write("Enter a task: ");
    }

    public string? ReadText() {
        return Console.ReadLine();
    }

    public void ListTasks(List<Todo> tasks) {
        Console.WriteLine();
        Console.WriteLine("Tasks:");
        for (var i = 0; i < tasks.Count; i++) {
            Console.WriteLine($"{i + 1}: {(tasks[i].Done ? "X " : "  ")}{tasks[i].Text}");
        }
    }

    public void PromptForATaskNumber() {
        Console.WriteLine();
        Console.Write("Enter the number of a task: ");
    }

    public string? ReadTaskNumber() {
        return Console.ReadLine();
    }
}