﻿namespace todo;

class Program
{
    static void Main(string[] args) {
        var todoStore = new TodoStore();
        var todoList = new TodoList(todoStore);
        var consoleUi = new ConsoleUi(todoList);
        todoList.SetConsoleUi(consoleUi);
        consoleUi.Run();
    }
}