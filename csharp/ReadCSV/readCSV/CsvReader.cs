﻿namespace readCSV;

public class CsvReader
{
    public IEnumerable<Record> ReadCsvFile()
    {
        var lines = File.ReadLines("people.csv");
        
        foreach (var line in lines)
        {
            var values = line.Split(";");
            var record = new Record(values);
            yield return record;
        }
    }
}

public record Record(string[] Values);
