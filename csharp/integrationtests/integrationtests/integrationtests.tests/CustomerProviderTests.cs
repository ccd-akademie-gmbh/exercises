using NUnit.Framework;

namespace integrationtests.tests;

[TestFixture]
public class CustomerProviderTests : IntegrationTestsBase
{
    [Test]
    public async Task Insert_Customer() {
        var sut = new CustomerProvider();
        var id = await sut.Add(TestData.Customers.Customer1);
        var result = await sut.GetAll();
        
        Assert.That(result.Count, Is.EqualTo(1));
        Assert.That(id, Is.EqualTo("0001"));
    }
}