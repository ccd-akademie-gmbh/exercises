﻿using System.Threading.Tasks;
using NUnit.Framework;
using VerifyNUnit;

namespace SupermarketReceipt.Test;

[TestFixture]
public class SupermarketApprovalTests
{
    [Test]
    public async Task Printed_receipt_should_not_change_during_refactoring_using_serialization()
    {
        // ARRANGE
        SupermarketCatalog catalog = new FakeCatalog();
        var toothbrush = new Product("toothbrush", ProductUnit.Each);
        catalog.AddProduct(toothbrush, 0.99);
        var apples = new Product("apples", ProductUnit.Kilo);
        catalog.AddProduct(apples, 1.99);

        var cart = new ShoppingCart();
        cart.AddItemQuantity(apples, 2.5);

        var teller = new Teller(catalog);
        teller.AddSpecialOffer(SpecialOfferType.TenPercentDiscount, toothbrush, 10.0);

        // ACT
        var receipt = teller.ChecksOutArticlesFrom(cart);
        
        // PRINT
        var result = ReceiptPrinter.PrintReceipt(receipt);

        // VERIFY
        await Verifier.Verify(result);
    }
}