﻿namespace customermanagement;

public class Customer
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string FirstName { get; set; }
}