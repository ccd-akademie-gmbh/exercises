﻿namespace customermanagement;

public class CustomerInserter
{
    private ICustomerRepository _customerRepository;

    public CustomerInserter(ICustomerRepository customerRepository)
    {
        _customerRepository = customerRepository;
    }

    public void HandleNewCustomer(Customer customer)
    {
        if (customer.Name == "")
        {
            throw new Exception("Customer name must not be empty");
        }

        customer.Id = Guid.NewGuid().ToString();

        try
        {
            _customerRepository.Insert(customer);
        }
        catch (Exception e)
        {
            throw new Exception("Insert failed", e);
        }
    }
}