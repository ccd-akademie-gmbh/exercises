namespace customermanagement;

public interface ICustomerRepository
{
    void Insert(Customer customer);
}