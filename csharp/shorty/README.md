# Mutation Testing

1. dotnet-stryker installieren:
- dotnet tool install -g dotnet-stryker


2. In das Test Verzeichnis wechseln
- cd mutation.tests

3. Stryker aufrufen
- dotnet-stryker

4. Tests ergänzen, bis alle Mutanten durch Tests abgedeckt sind
