using NUnit.Framework;

namespace mutation.tests
{
    public class ShortyTests
    {
        [Test]
        public void Shorter_string_is_returned_as_is() {
            Assert.That(Shorty.Shorten("A", 5), Is.EqualTo("A"));
        }
    }
}