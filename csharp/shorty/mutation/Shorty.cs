﻿namespace mutation
{
    public static class Shorty
    {
        public static string Shorten(string s, int length) {
            if (s.Length <= length) {
                return s;
            }

            if (length < 7) {
                return s.Remove(length);
            }
            
            var sampleLength = (length - 3) / 2;
            var extraLength = 0;
            if (sampleLength * 2 + 3 < length) {
                extraLength = 1;
            }
            var prefix = s.Substring(0, sampleLength + extraLength);
            var suffix = s.Remove(0, s.Length - sampleLength);
            var result = $"{prefix}...{suffix}";
            return result;
        }
    }
}
