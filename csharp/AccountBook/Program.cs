using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Account account = new Account();
        account.Run(new List<string>(args));
    }
}