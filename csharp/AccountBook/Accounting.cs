using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

public class Account
{
    private List<Booking> m_entries;
    private char m_delimiter;
    private string m_filename;
    private string m_depositName;
    private string m_balanceName;

    public Account()
    {
        m_entries = new List<Booking>();
        m_delimiter = ':';
        m_filename = "Account.txt";
        m_depositName = "deposit";
        m_balanceName = "balance";
    }

    public void Run(List<string> args)
    {
        if (args.Count == 0)
        {
            Console.WriteLine("Usage\n"
                            + "out [name] [amount]\n"
                            + "in [amount]\n"
                            + "overview\n\n"
                            + "e.g. Acounting.exe in 250 out 'Bowling' 25.32 out Dinner 64.97 overview");
            return;
        }

        while (args.Count > 0)
        {
            if (args[0] == "in")
            {
                Load();
                m_entries.Add(Booking.FromArgs(new List<string> { m_depositName, args[1] }, m_delimiter));
                Save();
                var e = new Booking { Name = m_balanceName, Amount = Total(), Time = DateTime.Now };
                e.Print(Console.Out);
                args.RemoveRange(0, 2);
            }
            else if (args[0] == "out")
            {
                Load();
                m_entries.Add(Booking.FromArgs(new List<string> { args[1], args[2] }, m_delimiter));
                Save();
                m_entries.Last().Print(Console.Out);
                var e = new Booking { Name = m_balanceName, Amount = Total(), Time = DateTime.Now };
                e.Print(Console.Out);
                args.RemoveRange(0, 3);
            }
            else if (args[0] == "overview")
            {
                Load();
                Print();
                args.RemoveAt(0);
            }
            else
            {
                Console.WriteLine($"Unknown command {args[0]}");
                args.RemoveAt(0);
            }
        }
    }

    public void Load()
    {
        if (File.Exists(m_filename))
        {
            m_entries.Clear();
            foreach (var line in File.ReadAllLines(m_filename))
            {
                var booking = new Booking();
                booking.Load(line, m_delimiter);
                m_entries.Add(booking);
            }
        }
    }

    public void Print()
    {
        Console.WriteLine("Account entries:");
        foreach (var e in m_entries)
        {
            e.Print(Console.Out);
        }
        var totalEntry = Booking.FromArgs(new List<string> { m_balanceName, Total().ToString() }, m_delimiter);
        totalEntry.Print(Console.Out);
    }

    public void Save()
    {
        using (var file = new StreamWriter(File.Open(m_filename, System.IO.FileMode.Create)))
        {
            foreach (var e in m_entries)
            {
                e.Save(file, m_delimiter);
            }
        }
    }

    public double Total()
    {
        double total = 0.0;
        foreach (var e in m_entries)
        {
            if (e.Name == m_depositName)
                total += e.Amount;
            else
                total -= e.Amount;
        }
        return total;
    }
}
