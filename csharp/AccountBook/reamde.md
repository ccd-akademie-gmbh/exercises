Exercise for Single Responsibility Principle:

Separation according to technical responsibilities is often difficult, as these often extend across several classes. Examine the accounting app and create a list of technical responsibilities. Then extract each of these responsibilities into its own class.