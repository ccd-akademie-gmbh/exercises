using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

public struct Booking
{
    public string Name { get; set; }
    public double Amount { get; set; }
    public DateTime Time { get; set; }

    public static Booking FromArgs(List<string> args, char delim)
    {
        var now = DateTime.Now;
        var name = args[0].Replace(delim, ';');
        var amount = double.Parse(args[1]);
        return new Booking { Name = name, Amount = amount, Time = now };
    }

    public void Print(TextWriter output)
    {
        output.WriteLine($"{Time.ToString("F", CultureInfo.InvariantCulture),-30} {Name,-20} = {Amount,10}");
    }

    public void Load(string line, char delim)
    {
        var parts = line.Split(delim);
        Name = parts[0];
        Amount = double.Parse(parts[1]);
        var ticks = Int64.Parse(parts[2]);
        Time = new DateTime(ticks);
    }

    public void Save(TextWriter output, char delim)
    {
        output.WriteLine($"{Name}{delim}{Amount}{delim}{Time.Ticks}");
    }
}
