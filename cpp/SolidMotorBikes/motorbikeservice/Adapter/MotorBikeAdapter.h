﻿#pragma once

#include "IMotorBike.h"

namespace motorbikeservice {
	namespace adapter {
		// KISS
		class BikeAdapter {
		public:
            interfaces::IUnknown* Bike;

			BikeAdapter(interfaces::IUnknown* motorbike) {
				Bike = motorbike;
			}
		};
	}
}