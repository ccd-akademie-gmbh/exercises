﻿#pragma once

namespace motorbikeservice {
    namespace interfaces {
		struct IUnknown {
			virtual ~IUnknown() = default;
		};

		// SRP
		// ISP
		struct IMotorBike : IUnknown{
		public:
			virtual double CalcServiceBill(const double* priceList, int count) = 0;
			virtual double RideByDay(double speed, double hours) = 0;
			virtual double RideByYear(double speed, double hours) = 0;
			virtual double GetTotalDistance() = 0;
			virtual void PrintBill(const std::string& paymentData) = 0;
			virtual double Calculate() = 0;
			virtual int CalculateCompetitionCounter() = 0;
		};
	}
}