﻿#pragma once
#include <vector>
#include <string>
#include "IMotorBike.h"

namespace motorbikeservice {

    class MotorbikeService {
        std::vector<interfaces::IUnknown*> _motorbikes;

    public:
        MotorbikeService() = default;

        void PrintBill(interfaces::IUnknown* obj, const std::string& paymentSlip);

        double Calculate(interfaces::IUnknown* obj);

        double RideByDay(interfaces::IUnknown* obj, double speed, double hours);

        double RideByYear(interfaces::IUnknown* obj, double speed, double hours);

        double GetTotalDistance(interfaces::IUnknown* obj);

        double CalcServiceBill(interfaces::IUnknown* obj, const double* priceList, int count);

        int CalculateCompetitionCounter(interfaces::IUnknown* obj);

        std::vector<interfaces::IUnknown*> GetAll();
    };
}
