#include "MotorbikeService.h"
#include "Chopper.h"
#include "Enduro.h"
#include "RacingBike.h"
#include <iostream>
#include <algorithm>

namespace motorbikeservice
{
    void MotorbikeService::PrintBill(interfaces::IUnknown* obj, const std::string& paymentSlip)
    {
        if (std::find(_motorbikes.begin(), _motorbikes.end(), obj) == _motorbikes.end()) {
            _motorbikes.push_back(obj);
        }

        if (Chopper* bike1 = dynamic_cast<Chopper*>(obj)) {
            bike1->PrintBill(paymentSlip);
        } else if (Enduro* bike2 = dynamic_cast<Enduro*>(obj)) {
            bike2->PrintBill(paymentSlip);
        } else if (RacingBike* bike = dynamic_cast<RacingBike*>(obj)) {
            bike->PrintBill(paymentSlip);
        }
    }

    double MotorbikeService::Calculate(interfaces::IUnknown* obj)
    {
        if (std::find(_motorbikes.begin(), _motorbikes.end(), obj) == _motorbikes.end()) {
            _motorbikes.push_back(obj);
        }

        if (Chopper* bike = dynamic_cast<Chopper*>(obj)) {
            return bike->Calculate();
        }

        if (Enduro* bike2 = dynamic_cast<Enduro*>(obj)) {
            return bike2->Calculate();
        }

        if (RacingBike* bike1 = dynamic_cast<RacingBike*>(obj)) {
            return bike1->Calculate();
        }

        return 0;
    }

    double MotorbikeService::RideByDay(interfaces::IUnknown* obj, double speed, double hours)
    {
        if (std::find(_motorbikes.begin(), _motorbikes.end(), obj) == _motorbikes.end()) {
            _motorbikes.push_back(obj);
        }

        if (Chopper* bike = dynamic_cast<Chopper*>(obj)) {
            return bike->RideByDay(speed, hours);
        }

        if (Enduro* bike2 = dynamic_cast<Enduro*>(obj)) {
            return bike2->RideByDay(speed, hours);
        }

        if (RacingBike* bike1 = dynamic_cast<RacingBike*>(obj)) {
            return bike1->RideByDay(speed, hours);
        }

        return 0;
    }

    double MotorbikeService::RideByYear(interfaces::IUnknown* obj, double speed, double hours)
    {
        if (std::find(_motorbikes.begin(), _motorbikes.end(), obj) == _motorbikes.end()) {
            _motorbikes.push_back(obj);
        }

        if (Chopper* bike = dynamic_cast<Chopper*>(obj)) {
            return bike->RideByYear(speed, hours);
        }

        if (Enduro* bike2 = dynamic_cast<Enduro*>(obj)) {
            return bike2->RideByYear(speed, hours);
        }

        if (RacingBike* bike1 = dynamic_cast<RacingBike*>(obj)) {
            return bike1->RideByYear(speed, hours);
        }

        return 0;
    }

    double MotorbikeService::GetTotalDistance(interfaces::IUnknown* obj)
    {
        if (std::find(_motorbikes.begin(), _motorbikes.end(), obj) == _motorbikes.end()) {
            _motorbikes.push_back(obj);
        }

        if (Chopper* bike = dynamic_cast<Chopper*>(obj)) {
            return bike->GetTotalDistance();
        }

        if (Enduro* bike2 = dynamic_cast<Enduro*>(obj)) {
            return bike2->GetTotalDistance();
        }

        if (RacingBike* bike1 = dynamic_cast<RacingBike*>(obj)) {
            return bike1->GetTotalDistance();
        }

        return 0;
    }

    double MotorbikeService::CalcServiceBill(interfaces::IUnknown* obj, const double* priceList, int count)
    {
        if (std::find(_motorbikes.begin(), _motorbikes.end(), obj) == _motorbikes.end()) {
            _motorbikes.push_back(obj);
        }

        if (Chopper* bike = dynamic_cast<Chopper*>(obj)) {
            return bike->CalcServiceBill(priceList, count);
        }

        if (Enduro* bike2 = dynamic_cast<Enduro*>(obj)) {
            return bike2->CalcServiceBill(priceList, count);
        }

        if (RacingBike* bike1 = dynamic_cast<RacingBike*>(obj)) {
            return bike1->CalcServiceBill(priceList, count);
        }

        return 0;
    }

    int MotorbikeService::CalculateCompetitionCounter(interfaces::IUnknown* obj)
    {
        if (std::find(_motorbikes.begin(), _motorbikes.end(), obj) == _motorbikes.end()) {
            _motorbikes.push_back(obj);
        }

        if (Chopper* bike = dynamic_cast<Chopper*>(obj)) {
            return bike->CalculateCompetitionCounter();
        }

        if (Enduro* bike2 = dynamic_cast<Enduro*>(obj)) {
            return bike2->CalculateCompetitionCounter();
        }

        if (RacingBike* bike1 = dynamic_cast<RacingBike*>(obj)) {
            return bike1->CalculateCompetitionCounter();
        }

        return 0;
    }

    std::vector<interfaces::IUnknown*> MotorbikeService::GetAll()
    {
        return _motorbikes;
    }
}