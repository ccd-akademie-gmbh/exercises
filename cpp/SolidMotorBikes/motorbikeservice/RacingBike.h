﻿#pragma once
#include <iostream>
#include "IMotorBike.h"

namespace motorbikeservice {

    class RacingBike : public interfaces::IMotorBike {
        int _competitionCounter;
        double _distance;

    public:
        std::string Name;
        std::string Model;
        double StayingQuality;

        RacingBike(const std::string& name, const std::string& model)
            : Name(name), Model(model), _distance(0), _competitionCounter(0), StayingQuality(1) {}

        void PrintBill(const std::string& paymentData) override {
            std::cout << paymentData << std::endl;
        }

        double RideByDay(double speed, double hours) override {
            _distance += speed * hours;
            _distance += speed * hours;
            return speed * hours;
        }

        double RideByYear(double speed, double hours) override {
            _distance += speed * hours;
            _distance += speed * hours;
            return speed * hours;
        }

        double GetTotalDistance() override {
            return _distance;
        }

        double Calculate() override {
            return StayingQuality;
        }

        int CalculateCompetitionCounter() override {
            _distance += 350;
            _competitionCounter++;
            return _competitionCounter;
        }

        double CalcServiceBill(const double* priceList, int count) override {
            double sum = 0;

            for (int i = 0; i < count; i++) {
                sum += priceList[i];
            }

            return sum;
        }

        double Distance() const {
            return _distance;
        }
        void SetDistance(double distance) {
            _distance = distance;
            StayingQuality = _distance / 100000 + 1;
        }
    };
}
