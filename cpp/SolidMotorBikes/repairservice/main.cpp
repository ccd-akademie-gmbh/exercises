﻿#include <iostream>
#include "MotorbikeService.h"
#include "Enduro.h"
#include "RacingBike.h"
#include "Chopper.h"

using namespace motorbikeservice;

int main() {
    MotorbikeService motorbikeService;
    Enduro enduro("Honda", "X250");
    RacingBike racingBike("Kawasaki", "H2R");
    Chopper chopper("Boulevard", "C90");

    motorbikeService.CalculateCompetitionCounter(&enduro);
    motorbikeService.CalculateCompetitionCounter(&racingBike);
    motorbikeService.CalculateCompetitionCounter(&chopper);

    for (auto motorBike : motorbikeService.GetAll()) {
        if (Chopper* bike = dynamic_cast<Chopper*>(motorBike)) {
            std::cout << bike->Distance() << std::endl;
        }

        if (Enduro* bike2 = dynamic_cast<Enduro*>(motorBike)) {
            std::cout << bike2->Distance() << std::endl;
        }

        if (RacingBike* bike1 = dynamic_cast<RacingBike*>(motorBike)) {
            std::cout << bike1->Distance() << std::endl;
        }
    }

    return 0;
}
