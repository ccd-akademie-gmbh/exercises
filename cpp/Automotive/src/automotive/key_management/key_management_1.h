#pragma once

#include <cstdint>
#include "key_management_types.h"

extern uint8_t HSM_get_random_byte();
namespace key_management {
    void create_key(uint8_t *key, uint8_t length);
    void create_nonce(uint8_t *nonce, uint8_t length);
}