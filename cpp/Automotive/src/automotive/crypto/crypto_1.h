#pragma once

#include <stdint.h>

#include "crypto_types.h"

extern uint8_t third_party_library_calc_hmac(const uint8_t* message, int len, const uint8_t* key, const uint8_t* nonce, uint8_t *hmac);

namespace crypto {
    void init();
    State get_state();
    Return_status set_key(Key key);
    Return_status set_nonce(Nonce nonce);
    Return_status calculate_hmac(const uint8_t *message, int len, Hmac *hmac);
    Return_status verify_hmac(const uint8_t *message, int len, Hmac *hmac);
}