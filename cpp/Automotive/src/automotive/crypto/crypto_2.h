#pragma once

#include "crypto_1.h"
#include "crypto_types.h"

namespace crypto {
    Return_status verify_nonce(Nonce *nonce);

    Return_status verify_key(Key key);
}