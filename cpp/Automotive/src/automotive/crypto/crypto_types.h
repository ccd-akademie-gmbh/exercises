#pragma once

#include <cstdint>

namespace crypto
{
    constexpr int NONCE_LENGTH = 64;
    constexpr int KEY_LENGTH = 64;
    constexpr int HMAC_LENGTH = 64;

    enum class State {
      uninitialized = 0,
      initialized,
      key_set,
      nonce_set,
      nonce_and_key_set
    };

    struct Nonce {
      uint8_t nonce[NONCE_LENGTH];
      int time_of_creation;
    };

    struct Key {
      uint8_t key[KEY_LENGTH];
    };

    struct Hmac {
      uint8_t hmac[HMAC_LENGTH];
    };

    enum class Return_status {
      valid_key_provided,
      invalid_key_provided,
      valid_nonce_provided,
      invalid_nonce_provided,
      wrong_state,
      hmac_successfully_calculated,
      error_during_hmac_calculation,
      invalid_hmac,
      valid_hmac
    };
}