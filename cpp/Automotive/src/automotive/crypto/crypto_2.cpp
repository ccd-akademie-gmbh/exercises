#include "crypto_2.h"
#include "crypto_types.h"

#include "time_1.h"

#include <cstdint>
namespace crypto {
    Return_status verify_nonce(Nonce *nonce) {
        for (uint8_t i : nonce->nonce) {
            if (i != 0) {
                int64_t ct = current_time();
                if (ct > INT64_MIN + 300 && nonce->time_of_creation > ct - 300) {
                    return Return_status::valid_nonce_provided;
                }
            }
        }
        // Nonce is only zero
        return Return_status::invalid_nonce_provided;
    }

    Return_status verify_key(Key key) {
        for (uint8_t k : key.key) {
            if (k != 0) {
                return Return_status::valid_key_provided;
            }
        }
        // Key is only zero
        return Return_status::invalid_key_provided;
    }
}