#pragma once
#include "gps_types.h"

namespace gps {
    extern uint8_t Driver_obtain_current_position(uint8_t *position_as_bytes, uint8_t *hmac_as_bytes);

    Return_status init_crypto_module();

    Return_status set_destination_postition(Position position);

    Position get_destination_position();

    Return_status get_current_position(Position *position);
}