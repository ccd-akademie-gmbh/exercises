#pragma once

#include <cstdint>

namespace gps {
    struct Position {
        uint8_t longitude_degree;
        uint8_t longitude_minute;
        uint8_t longitude_second;
        uint8_t latitude_degree;
        uint8_t latitude_minute;
        uint8_t latitude_second;
    };

    enum class Return_status {
        Success,
        Failure,
    };
}