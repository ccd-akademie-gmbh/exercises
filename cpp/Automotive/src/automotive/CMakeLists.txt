add_library(automotive
        crypto/crypto_1.cpp
        crypto/crypto_2.cpp
        gps/gps_1.cpp
        key_management/key_management_1.cpp
        time/time_1.cpp
)

target_include_directories(automotive PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/crypto
    ${CMAKE_CURRENT_SOURCE_DIR}/gps
    ${CMAKE_CURRENT_SOURCE_DIR}/key_management
    ${CMAKE_CURRENT_SOURCE_DIR}/time
)
set_target_properties(automotive PROPERTIES FOLDER "Automotive")