cmake_minimum_required(VERSION 3.16)

project(cpp-testing CXX)

# Export compilation database
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_subdirectory(src/automotive)
add_subdirectory(src/automotive.tests)