#pragma once
#include <string>

class Shorty 
{
public:
    static std::string Shorten(const std::string& s, int length);
};