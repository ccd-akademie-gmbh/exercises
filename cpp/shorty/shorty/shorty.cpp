#include "shorty.h"

std::string Shorty::Shorten(const std::string& s, int length)
{
	if(s.length() <= length) 
	{
		return s;
	}
	if(length < 7)
	{
		return s.substr(0, length);
	}
	int sampleLength = (length - 3) / 2;
	int extraLength = 0;
	if(sampleLength * 2 + 3 < length)
	{
		extraLength = 1;
	}
	auto prefix = s.substr(0, sampleLength + extraLength);
	auto suffix = s.substr(s.length() - sampleLength);
	return prefix + "..." + suffix;
}