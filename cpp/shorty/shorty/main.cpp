#include "shorty.h"
#include <iostream>

int main(int argc, char* argv[])
{
	int length = 10;
	if(argc == 1)
	{
		std::cout << "Usage: shorty ALongStringToShorten 6\nAL...n";
		return -1;
	}
	if(argc > 2)
		length = std::stoi(argv[2]);
	auto result = Shorty::Shorten(argv[1], length);
	std::cout << result << std::endl;
	return 0;
}