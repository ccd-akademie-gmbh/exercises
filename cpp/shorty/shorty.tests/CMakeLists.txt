set(TEST_NAME shorty.tests)

add_executable(${TEST_NAME})
target_sources(${TEST_NAME} PRIVATE shorty.tests.cpp)

target_link_libraries(${TEST_NAME} PRIVATE shorty.lib)
set_target_properties(${TEST_NAME} PROPERTIES FOLDER "Shorty")
AddTests(${TEST_NAME})