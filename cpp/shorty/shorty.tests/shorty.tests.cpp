#include "shorty.h"
#include <gmock/gmock.h>
#include <rapidcheck/gtest.h>

using namespace testing;

TEST(ShortyTests, Shorten_StringIsShortEnough_ReturnedAsIs)
{
	auto actual = Shorty::Shorten("ABC", 5);
	
	EXPECT_THAT(actual, StrEq("ABC"));
}

TEST(ShortyTests, Shorten_StringIsGreaterAndShortenToLessThen7_ReturnWithRemovedTrail)
{
	auto actual = Shorty::Shorten("ABCDEFG", 5);
	
	EXPECT_THAT(actual, StrEq("ABCDE"));
}