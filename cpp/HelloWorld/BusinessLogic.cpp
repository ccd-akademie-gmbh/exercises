#include "BusinessLogic.h"
#include "Templates.h"

std::string BusinessLogic::DetermineGreeting(const std::string& name, const std::map<std::string, int>& visits)
{
	Templates templates;
	auto found = visits.find(name);
	if(found != end(visits))
	{
		auto result = templates.Get(found->second);
		return result + name + "!";
	}
	return "Hello " + name + "!";
}
