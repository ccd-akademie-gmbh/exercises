#include <iostream>
#include "File.h"
#include "BusinessLogic.h"

int main(int argc, char* argv[])
{
	BusinessLogic logic;
	while(true)
	{
		std::cout << "Name: ";
		std::string name;
		std::getline(std::cin, name);
		File::AppendAllLines("guestlist.txt", {name});
		auto names = File::ReadAllLines("guestlist.txt");
		std::map<std::string, int> visits;
		for(const auto& name : names)
		{
			if(visits.find(name) == visits.end())
				visits.emplace(name, 1);
			else
				visits.at(name) += 1;
		}
		auto greeting = logic.DetermineGreeting(name, visits);
		std::cout << greeting << std::endl;
	}
}
