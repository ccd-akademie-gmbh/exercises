#pragma once
#include <filesystem>
#include <vector>
#include <string>

class File
{
public:
	static void AppendAllLines(const std::filesystem::path& filePath, const std::vector<std::string>& lines);
	static std::vector<std::string> ReadAllLines(const std::filesystem::path& filePath);
};
