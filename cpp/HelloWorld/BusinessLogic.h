#pragma once
#include <string>
#include <map>

class BusinessLogic
{
public:
	std::string DetermineGreeting(const std::string& name, const std::map<std::string, int>& visits);
};
