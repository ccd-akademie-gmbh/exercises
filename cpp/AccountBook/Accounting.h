#pragma once
#include <string>
#include <vector>
#include <chrono>
#include "Booking.h"

class Account
{
public:
	Account();
	void Run(std::vector<std::string> args);
	void Load();
	void Print() const;
	void Save() const;
	double Total() const;

private:
	std::vector<Booking> m_entries;
	char m_delimiter;
	std::string m_filename;
	std::string m_depositName;
	std::string m_balanceName;
};
