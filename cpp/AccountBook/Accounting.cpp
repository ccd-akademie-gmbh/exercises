#define _CRT_SECURE_NO_WARNINGS
#include "Accounting.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <ctime>


Booking Booking::FromArgs(const std::vector<std::string>& args, char delim)
{
	auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	auto name = args[0];
	//replace delimiter in name to avoid error during load
	std::replace(begin(name), end(name), delim, ';');
	auto amount = std::stod(args[1]);
	return {name, amount, now};
}

void Booking::Print(std::ostream& out) const
{
	out << std::left << std::put_time(std::localtime(&time), "%c") << std::right << std::setw(20) << name << " = " << amount << '\n';
}

void Booking::Load(std::string line, char delim)
{
	std::stringstream str(line);
	std::string temp;
	std::getline(str, temp, delim);
	name = move(temp);
	std::getline(str, temp, delim);
	amount = std::stod(temp);
	temp.clear();
	std::getline(str, temp, delim);
	time = std::stoll(temp);
}

void Booking::Save(std::ostream& out, char delim) const
{
	out << name << delim << amount << delim << time << '\n';
}

Account::Account()
	: m_delimiter(':')
	, m_filename("Account.txt")
	, m_depositName("deposit")
	, m_balanceName("balance")
{}

void Account::Run(std::vector<std::string> args)
{
	if(args.empty())
	{
		std::cout << "Usage\n"
				  << "out [name] [amount]\n"
				  << "in [amount]\n"
				  << "overview\n\n"
				  << "e.g. Acounting.exe in 250 out 'Bowlen gehen' 25.32 out Essen 64.97 overview";
	}
	while(!args.empty())
	{
		if(args[0] == "in")
		{
			Load();
			m_entries.emplace_back(Booking::FromArgs({m_depositName, args[1]}, m_delimiter));
			Save();
			Booking e{m_balanceName, Total(), m_entries.back().time};
			e.Print(std::cout);
			args.erase(begin(args), begin(args) + 2);
		}
		else if(args[0] == "out")
		{
			Load();

			m_entries.emplace_back(Booking::FromArgs({args[1], args[2]}, m_delimiter));
			Save();
			m_entries.back().Print(std::cout);
			Booking e{m_balanceName, Total(), m_entries.back().time};
			e.Print(std::cout);
			args.erase(begin(args), begin(args) + 3);
		}
		else if(args[0] == "overview")
		{
			Load();
			Print();
			args.erase(begin(args));
		}
		else
		{
			std::cerr << "unknown command " << args[0] << std::endl;
			args.erase(begin(args));
		}
	}
}

void Account::Load()
{
	std::ifstream file(m_filename);
	std::string line;
	while(std::getline(file, line))
	{
		Booking e{};
		e.Load(line, m_delimiter);
		m_entries.emplace_back(e);
	}
}

void Account::Print() const
{
	std::cout << "Account entries" << std::endl;
	for(const auto& e : m_entries)
		e.Print(std::cout);
	auto e = Booking::FromArgs({m_balanceName, std::to_string(Total())}, m_delimiter);
	e.Print(std::cout);
	std::cout << std::endl;
}

void Account::Save() const
{
	std::ofstream file(m_filename);
	for(const auto& e : m_entries)
		e.Save(file, m_delimiter);
}

double Account::Total() const
{
	double total = 0.0;
	for(const auto& [name, amount, _] : m_entries)
	{
		if(name == m_depositName)
			total += amount;
		else
			total -= amount;
	}
	return total;
}
