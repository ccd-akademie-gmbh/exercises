#pragma once

#include <map>
#include <string>

std::map<std::string, std::string> ToDictionary(const std::string& input);
