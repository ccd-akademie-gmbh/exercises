#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <stdexcept>


std::vector<std::string> SplitIntoSettings(const std::string& input);
std::vector<std::pair<std::string, std::string>> SplitIntoKeyAndValue(const std::vector<std::string>& settings);
std::map<std::string, std::string> CreateDictionary(const std::vector<std::pair<std::string, std::string>>& keyValuePairs);

std::map<std::string, std::string> ToDictionary(const std::string& input)
{
	auto settings = SplitIntoSettings(input);
	auto keyValuePairs = SplitIntoKeyAndValue(settings);
	auto dictionary = CreateDictionary(keyValuePairs);
	return dictionary;
}

std::vector<std::string> SplitIntoSettings(const std::string& input)
{
	std::vector<std::string> result;
	std::stringstream ss(input);
	std::string item;
	while(std::getline(ss, item, ';'))
	{
		result.push_back(item);
	}
	return result;
}

std::vector<std::pair<std::string, std::string>> SplitIntoKeyAndValue(const std::vector<std::string>& settings)
{
	std::vector<std::pair<std::string, std::string>> result;
	for(const auto& setting : settings)
	{
		std::stringstream ss(setting);
		std::string key, value;

		if(std::getline(ss, key, '='))
		{
			if(key.empty())
			{
				throw std::runtime_error("Key must not be empty");
			}
            std::getline(ss, value);
			result.emplace_back(key, value);
		}
	}
	return result;
}

std::map<std::string, std::string> CreateDictionary(const std::vector<std::pair<std::string, std::string>>& keyValuePairs)
{
	std::map<std::string, std::string> dictionary;
	for(const auto& pair : keyValuePairs)
	{
		dictionary[pair.first] = pair.second;
	}
	return dictionary;
}
