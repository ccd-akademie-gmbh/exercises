add_library(todictionary STATIC library.cpp library.h)
add_executable(todictionary.Tests library.tests.cpp)
target_link_libraries(todictionary.Tests PRIVATE todictionary)
AddTests(todictionary.Tests)

set_target_properties(todictionary PROPERTIES FOLDER "todictionary")
set_target_properties(todictionary.Tests PROPERTIES FOLDER "todictionary")