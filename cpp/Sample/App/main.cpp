#include "Interactor.h"

int main(int argc, char* argv[])
{
	Interactor::Run({argv, argv + argc});
	return 0;
}
