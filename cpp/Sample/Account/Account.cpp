#include "Account.h"

constexpr const char DEPOSIT_NAME[] = "deposit";

void Account::Deposit(double amount, long long time)
{
	Booking b = {DEPOSIT_NAME, amount, time};
	m_bookings.push_back(b);
}

Booking Account::Payout(const std::string& name, double amount, long long time)
{
	Booking b = {name, amount, time};
	m_bookings.push_back(b);
	return b;
}

double Account::Balance() const
{
	double total = 0.0;
	for(const auto& [name, amount, _] : m_bookings)
	{
		if(name == DEPOSIT_NAME)
			total += amount;
		else
			total -= amount;
	}
	return total;
}
