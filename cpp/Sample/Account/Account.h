#pragma once
#include "Booking.h"
#include <vector>

// encapsulate logic
class Account
{
public:
	void Deposit(double amount, long long time);
	Booking Payout(const std::string& name, double amount, long long time);
	double Balance() const;

	std::vector<Booking> m_bookings;
};
