#pragma once
#include <string>

struct Booking
{
	std::string name;
	double amount;
	long long time;
};
