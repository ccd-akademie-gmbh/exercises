#include <gmock/gmock.h>
#include <rapidcheck/gtest.h>
#include "Account.h"

std::ostream& operator<< (std::ostream& out, const Booking& b)
{
    out << b.name << ", " << b.amount << ", " << b.time;
    return out;
}
bool operator== (const Booking& left, const Booking& right)
{
    return left.name == right.name
        && left.amount == right.amount
        && left.time == right.time;
}
using namespace testing;

TEST(AccountTests, Deposit_Empty_Added)
{
    Account account;

    account.Deposit(100, 34);

    const std::vector<Booking> expected = {{"deposit", 100, 34}};
    EXPECT_THAT(account.m_bookings, expected);
}

TEST(AccountTests, Payout_Empty_Added)
{
    Account account;

    account.Payout("rent", 100, 34);

    const std::vector<Booking> expected = {{"rent", 100, 34}};
    EXPECT_THAT(account.m_bookings, expected);
}

TEST(AccountTests, Balance_SomePayouts_ReturnTotal)
{
    Account account;
    account.Payout("rent", 100, 34);
    account.Payout("rent", 100, 34);
    account.Payout("rent", 100, 34);

    auto balance = account.Balance();

    EXPECT_THAT(balance, -300);
}

RC_GTEST_PROP(AccountTests, Payout_Any_ReduceBalance, ())
{
	double amount = *rc::gen::nonNegative<double>();
	Account account;
	
	account.Payout("rent", amount, 5);
	
    RC_ASSERT(account.Balance() <= 0);
}

RC_GTEST_PROP(AccountTests, Deposit_Any_IncreaseBalance, ())
{
	double amount = *rc::gen::nonNegative<double>();
	Account account;

	account.Deposit(amount, 5);

	RC_ASSERT(account.Balance() >= 0);
}