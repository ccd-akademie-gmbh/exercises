set(TARGET SAccount.Tests)

add_executable(${TARGET})
target_sources(${TARGET} PRIVATE Account.Tests.cpp)

target_link_libraries(${TARGET} PRIVATE 
  SContracts 
  SAccount
)
set_target_properties(${TARGET} PROPERTIES FOLDER "Sample")
AddTests(${TARGET})