#include "Interactor.h"
#include "Account.h"
#include <filesystem>
#include <iostream>
#include <fstream>
#include <sstream>

std::vector<Booking> LoadBookings(const std::string& filename);
void Print(const Account& account);

void Interactor::Run(std::vector<std::string> args)
{
	if(args.size() == 1)
	{
		std::cout << "Usage: Accounting.exe Account.txt";
		return;
	}
	Account account = {LoadBookings(args[1])};
	Print(account);
}

constexpr const char DELIMITER = ':';

Booking ParseBooking(const std::string& line)
{
	Booking b{};
	std::stringstream str(move(line));
	std::string temp;
	std::getline(str, temp, DELIMITER);
	b.name = move(temp);
	std::getline(str, temp, DELIMITER);
	b.amount = std::stod(temp);
	temp.clear();
	std::getline(str, temp, DELIMITER);
	b.time = std::stoll(temp);
	return b;
}

std::vector<Booking> LoadBookings(const std::string& filename)
{
	std::vector<std::string> lines;
	std::fstream file(filename);
	std::string line;
	while(std::getline(file, line))
		lines.push_back(std::move(line));

	std::vector<Booking> bookings;
	for(const auto& l : lines)
	{
		auto b = ParseBooking(l);
		bookings.emplace_back(b);
	}
	return bookings;
}

void Print(const Account& account)
{
	std::cout << "Bookings\n";
	for(const auto& b : account.m_bookings)
	{
		std::cout << std::setw(20) << b.name << " = " << b.amount << std::endl;
	}
	auto total = account.Balance();
	std::cout << std::setw(20) << "balance = " << total << std::endl << std::endl;
}