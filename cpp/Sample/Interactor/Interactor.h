#pragma once
#include <vector>
#include <string>

// integration module
class Interactor
{
public:
	static void Run(std::vector<std::string> args);
};
