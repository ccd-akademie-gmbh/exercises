set(LIB_NAME SInteractor)
add_library(${LIB_NAME} STATIC)
target_sources(${LIB_NAME} PRIVATE
  Interactor.cpp
  Interactor.h
)

target_link_libraries(${LIB_NAME} PRIVATE
  SContracts
  SAccount
)

target_include_directories(${LIB_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
set_target_properties(${LIB_NAME} PROPERTIES FOLDER "Sample")