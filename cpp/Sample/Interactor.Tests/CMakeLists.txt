set(TARGET SInteractor.Tests)

add_executable(${TARGET})
target_sources(${TARGET} PRIVATE
  Interactor.Tests.cpp
)

target_link_libraries(${TARGET} PRIVATE 
  SInteractor
)
set_target_properties(${TARGET} PROPERTIES FOLDER "Sample")
AddTests(${TARGET})