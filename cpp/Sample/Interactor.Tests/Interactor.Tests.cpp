#include "Interactor.h"
#define APPROVALS_GOOGLETEST
#include "approvaltests.hpp"
#include "gmock/gmock.h"
#include <fstream>
#include <sstream>
#include <filesystem>
#include <string>

using namespace testing;
using namespace ApprovalTests;
// This puts "received" and "approved" files in approval_tests/ sub-directory,
// keeping the test source directory tidy:
auto directoryDisposer = ApprovalTests::Approvals::useApprovalsSubdirectory("approval_tests");

struct InteractorTests : Test
{
	void SetupAccountFile()
	{
		std::ofstream file("Account.txt");
		file << "deposit:800:12489182498\nEssen:50:12489182798\nWaschmaschine:250.99:12489183498\n"
			 << "deposit:200:12489185498\nAuto lackieren:560:12489187498\nBahnfahrt:49.99:12489282498\n";
	}
	void RedirectConsoleOutput()
	{
		m_originalBuffer = std::cout.rdbuf(m_consoleOutput.rdbuf());
	}
	std::string DeleteAccountFileAndReturnContent()
	{
		std::string result;
		std::ifstream file("Account.txt");
		std::string line;
		while(std::getline(file, line))
		{
			result.append(std::move(line));
			result.push_back('\n');
		}
		file.close();
		std::filesystem::remove("Account.txt");
		return result;
	}
	std::string ResetConsoleAndReturnOutput()
	{
		std::cout.rdbuf(m_originalBuffer);
		return m_consoleOutput.str();
	}
	std::stringstream m_consoleOutput;
	std::streambuf* m_originalBuffer;
};

TEST_F(InteractorTests, Overview_WithAlreadyExistingAccount_ShowCorrectOverview)
{
	SetupAccountFile();
	RedirectConsoleOutput();

	Interactor::Run({internal::GetArgvs()[0], "Account.txt"});

	auto content = ResetConsoleAndReturnOutput();
	EXPECT_THAT(content, EndsWith("balance = 89.02\n\n"));
	std::filesystem::remove("Account.txt");
}

TEST_F(InteractorTests, Overview_ApprovalWithAlreadyExistingAccount_WriteCorrectFile)
{
	SetupAccountFile();

	Interactor::Run({internal::GetArgvs()[0], "Account.txt"});

	auto content = DeleteAccountFileAndReturnContent();
	Approvals::verify(content);
} 