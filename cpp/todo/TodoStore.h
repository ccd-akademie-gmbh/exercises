#pragma once
#include <string>
#include <vector>

struct Todo
{
	std::string text;
	bool done;
};
using Todos = std::vector<Todo>;

class TodoStore
{
public:
	void Save(const Todos& todos);
	Todos Load();
	
private:
	Todos m_todos;
};