#pragma once

enum class Command
{
	None,
	Add,
	List,
	Done,
	Remove,
	Quit
};
