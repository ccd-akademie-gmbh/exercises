#include "ConsoleUi.h"
#include "TodoStore.h"
#include "TodoList.h"

int main()
{
	TodoStore store;
	TodoList list(&store);
	ConsoleUi console(&list);
	list.SetConsoleUi(&console);
	console.Run();
	return 0;
}
