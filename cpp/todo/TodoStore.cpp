#include "TodoStore.h"
#include <fstream>
#include <sstream>
#include <filesystem>

const std::string FILENAME = "todos.json";

std::string ToJson(const Todos& todos);
Todos FromJson(const std::string& json);

void TodoStore::Save(const Todos& todos)
{
	std::string content = ToJson(todos);
	std::ofstream file(FILENAME);
	file << content << std::endl;
}

Todos TodoStore::Load()
{
	if(std::filesystem::exists(FILENAME))
	{
		std::ifstream file(FILENAME);
		std::string content;
		std::string line;
		while(std::getline(file, line))
			content.append(std::move(line));
		m_todos = FromJson(content);
	}
	return m_todos;
}

std::string ToJson(const Todos& todos)
{
	std::stringstream out;
	out << '{';
	for(const Todo& t : todos)
	{
		out << "Todo: {"
			<< "text: " << t.text << " ,"
			<< "done: " << t.done << " },";
	}
	out << '}';
	return out.str();
}

Todos FromJson(const std::string& json)
{
	Todos todos;
	std::stringstream in(json);
	in.ignore(); //{
	std::string name;
	in >> name;
	while(name == "Todo:")
	{
		Todo t;
		in.ignore(2); // {
		in >> name >> t.text;
		in >> name >> t.done;
		in.ignore(3); // },
		in >> name;
		todos.push_back(t);
	}
	return todos;
}
