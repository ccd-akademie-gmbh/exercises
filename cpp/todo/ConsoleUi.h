#pragma once
#include "Command.h"
#include <string>
#include "TodoStore.h"

class TodoList;

class ConsoleUi
{
public:
	ConsoleUi(TodoList*);
	void Run();
	void PromptForATask();
	std::string ReadText();
	void ListTasks(const Todos&);
	void PromptForATaskNumber();
	std::string ReadTaskNumber();

private:
	TodoList* m_list;
};
