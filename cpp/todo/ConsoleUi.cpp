#include "ConsoleUi.h"
#include "TodoList.h"
#include <iostream>

Command GetCommand();
Command CommandFromInput(const std::string&);
std::string ReadLine();

ConsoleUi::ConsoleUi(TodoList* l)
	: m_list(l)
{}

void ConsoleUi::Run()
{
	Command command;
	do
	{
		command = GetCommand();
		switch(command)
		{
		case Command::Add: m_list->Add(); break;
		case Command::List: m_list->List(); break;
		case Command::Done: m_list->Done(); break;
		case Command::Remove: m_list->Remove(); break;
		}
	} while(command != Command::Quit);
}

void ConsoleUi::PromptForATask()
{
	std::cout << "Enter a task: ";
}

std::string ConsoleUi::ReadText()
{
	return ReadLine();
}

void ConsoleUi::ListTasks(const Todos& todos)
{
	std::cout << "\nTasks:\n";
	int index = 0;
	for(const Todo& t : todos)
	{
		std::cout << ++index << ": "
				  << (t.done ? 'X' : ' ')
				  << ' ' << t.text << '\n';
	}
}

void ConsoleUi::PromptForATaskNumber()
{
	std::cout << "\nEnter the number of a task: ";
}

std::string ConsoleUi::ReadTaskNumber()
{
	return ReadLine();
}

Command GetCommand()
{
	Command command;
	do
	{
		std::cout << "\nA)dd L)ist D)one R)emove Q)uit\n"
				  << "Enter a command: ";
		std::string input = ReadLine();
		command = CommandFromInput(input);
	} while(command == Command::None);
	return command;
}

Command CommandFromInput(const std::string& input)
{
	if(input.empty())
		return Command::None;

	switch(tolower(input.front()))
	{
	case 'a': return Command::Add;
	case 'l': return Command::List;
	case 'd': return Command::Done;
	case 'r': return Command::Remove;
	case 'q': return Command::Quit;
	}
	return Command::None;
}

std::string ReadLine()
{
	std::string line;
	std::getline(std::cin, line);
	return line;
}
