#pragma once
#include "TodoStore.h"

class ConsoleUi;

class TodoList
{
public:
	TodoList(TodoStore*);
	void SetConsoleUi(ConsoleUi*);
	void Add();
	void List();
	void Done();
	void Remove();

private:
	Todos m_tasks;
	TodoStore* m_store;
	ConsoleUi* m_console = nullptr;
};
