#include "TodoList.h"
#include "ConsoleUi.h"

TodoList::TodoList(TodoStore* s)
	: m_tasks(s->Load())
	, m_store(s)
{}

void TodoList::SetConsoleUi(ConsoleUi* c)
{
	m_console = c;
}

void TodoList::Add()
{
	m_console->PromptForATask();
	auto text = m_console->ReadText();
	if(!text.empty())
	{
		m_tasks.push_back(Todo{text, false});
		m_store->Save(m_tasks);
	}
}

void TodoList::List()
{
	m_console->ListTasks(m_tasks);
}

void TodoList::Done()
{
	m_console->ListTasks(m_tasks);
	m_console->PromptForATaskNumber();
	std::string input = m_console->ReadTaskNumber();
	int index = std::stoi(input);
	if(index > 0 && index <= m_tasks.size())
	{
		m_tasks[index - 1].done = true;
		m_store->Save(m_tasks);
	}
}

void TodoList::Remove()
{
	m_console->ListTasks(m_tasks);
	m_console->PromptForATaskNumber();
	std::string input = m_console->ReadTaskNumber();
	int index = std::stoi(input);
	if(index > 0 && index <= m_tasks.size())
	{
		m_tasks.erase(begin(m_tasks) + index - 1);
		m_store->Save(m_tasks);
	}
}
