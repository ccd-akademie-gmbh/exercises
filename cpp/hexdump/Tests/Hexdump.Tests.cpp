#include "Hexdump.h"
#include <gmock/gmock.h>
#define APPROVALS_GOOGLETEST
#include "approvaltests.hpp"

using namespace testing;
using namespace ApprovalTests;

// use 'approvals' folder for approval files
auto directoryDisposer = ApprovalTests::Approvals::useApprovalsSubdirectory("approvals");

class HexdumpTests : public Test
{
public:
	void RedirectConsoleOutput(std::ostream& out)
	{
		m_originalBuffer = out.rdbuf(m_consoleOutput.rdbuf());
	}
	std::string ResetConsoleAndReturnOutput(std::ostream& out)
	{
		out.rdbuf(m_originalBuffer);
		return m_consoleOutput.str();
	}

	int HexdumpUT(int argc, const char* argv[])
	{
		int ret = -1;
		//TODO ret = main(argc, argv); How to execute the code this?
		return ret;
	}

	std::stringstream m_consoleOutput;
	std::streambuf* m_originalBuffer;
};

TEST_F(HexdumpTests, main_ValidFile_Dump)
{
	RedirectConsoleOutput(std::cout);
	const char* argv[] = {"pathToExe", "example.bin"};

	int ret = HexdumpUT(std::size(argv), argv);

	std::string output = ResetConsoleAndReturnOutput(std::cout);
	//TODO How to test the output text?
	EXPECT_THAT(ret, Eq(0));
}

TEST_F(HexdumpTests, main_NoFile_ReturnUsageMessage)
{
	RedirectConsoleOutput(std::cerr);
	const char* argv[] = {"pathToExe"};

	int ret = HexdumpUT(std::size(argv), argv);

	std::string output = ResetConsoleAndReturnOutput(std::cerr);
	//TODO How to test the output text?
	EXPECT_THAT(ret, Eq(1));
}

TEST_F(HexdumpTests, main_NotExistingFile_ReturnInvalidFileMessage)
{
	RedirectConsoleOutput(std::cerr);
	const char* argv[] = {"pathToExe", "FileDoesNotExists.bin"};

	int ret = HexdumpUT(std::size(argv), argv);

	std::string output = ResetConsoleAndReturnOutput(std::cerr);
	//TODO How to test the output text?
	EXPECT_THAT(ret, Eq(2));
}
