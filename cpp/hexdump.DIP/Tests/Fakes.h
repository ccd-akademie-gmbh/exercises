#pragma once
#include "IFileProvider.h"
#include "IConsole.h"
#include "ICommandParser.h"

struct FileProviderFake : IFileProvider
{
	MOCK_METHOD(bool, Open, (const std::string& filename), (override));
	//TODO complete define fake class methods
};

struct ConsoleFake : IConsole
{
	//TODO define fake class methods
};

struct CommandParserFake : ICommandParser
{
	//TODO define fake class methods
};
