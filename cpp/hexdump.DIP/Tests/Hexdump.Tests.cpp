#include "Hexdump.h"
#include "FileProvider.h"
#include "Console.h"
#include "CommandParser.h"
#include <gmock/gmock.h>
#define APPROVALS_GOOGLETEST
#include "approvaltests.hpp"
#include "Fakes.h"

using namespace testing;
using namespace ApprovalTests;

// use 'approvals' folder for approval files
auto directoryDisposer = ApprovalTests::Approvals::useApprovalsSubdirectory("approvals");

class HexdumpTests : public Test
{
public:
	HexdumpTests()
		: m_sut(&m_console, &m_command, &m_file)
		, m_originalBuffer(nullptr)
	{}
	void RedirectConsoleOutput(std::ostream& out)
	{
		m_originalBuffer = out.rdbuf(m_consoleOutput.rdbuf());
	}
	std::string ResetConsoleAndReturnOutput(std::ostream& out)
	{
		out.rdbuf(m_originalBuffer);
		return m_consoleOutput.str();
	}

	int HexdumpUT(int argc, const char* argv[])
	{
		int ret = -1;
		ret = m_sut.Process(argc, argv);
		return ret;
	}

	Console m_console;
	FileProvider m_file;
	CommandParser m_command;
	Hexdump m_sut;
	std::stringstream m_consoleOutput;
	std::streambuf* m_originalBuffer;
};

TEST_F(HexdumpTests, main_ValidFile_Dump)
{
	RedirectConsoleOutput(std::cout);
	const char* argv[] = {"pathToExe", "example.bin"};

	int ret = HexdumpUT(std::size(argv), argv);

	std::string output = ResetConsoleAndReturnOutput(std::cout);
	Approvals::verify(output);
	EXPECT_THAT(ret, Eq(0));
}

TEST_F(HexdumpTests, main_NoFile_ReturnUsageMessage)
{
	RedirectConsoleOutput(std::cerr);
	const char* argv[] = {"pathToExe"};

	int ret = HexdumpUT(std::size(argv), argv);

	std::string output = ResetConsoleAndReturnOutput(std::cerr);
	Approvals::verify(output);
	EXPECT_THAT(ret, Eq(1));
}

TEST_F(HexdumpTests, main_NotExistingFile_ReturnInvalidFileMessage)
{
	RedirectConsoleOutput(std::cerr);
	const char* argv[] = {"pathToExe", "FileDoesNotExists.bin"};

	int ret = HexdumpUT(std::size(argv), argv);

	std::string output = ResetConsoleAndReturnOutput(std::cerr);
	Approvals::verify(output);
	EXPECT_THAT(ret, Eq(2));
}
