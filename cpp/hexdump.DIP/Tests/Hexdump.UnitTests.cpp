#include "Hexdump.h"
#include <gmock/gmock.h>
#include "Fakes.h"

using namespace testing;
// We can now fine granular set parameters of the fakes to check the logic
class HexdumpMockTests : public Test
{
public:
	HexdumpMockTests()
		: m_sut(&m_console, &m_command, &m_file)
	{
		// Specify some default behaviour to avoid having to do it in every test
		ON_CALL(m_command, ParseForFile).WillByDefault(Return("default.bin"));
		ON_CALL(m_file, Open).WillByDefault(Return(true));
	}

	StrictMock<ConsoleFake> m_console;
	NiceMock<FileProviderFake> m_file;
	NiceMock<CommandParserFake> m_command;
	Hexdump m_sut;
};

TEST_F(HexdumpMockTests, Process_FileHasOneLine_ReturnHex)
{
	// do not need any valid input because we fake the parser in the constructor
	const char* argv[] = {"", "default.bin"};
	unsigned char content[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'};
	EXPECT_CALL(m_file, Read) // use EXPECT_CALL also on stubs to fine granular specify the behaviour
		.WillOnce(DoAll(SetArrayArgument<0>(content, content + 16), Return(16))) // fill buffer with content AND return 16
		.WillOnce(Return(0));
	EXPECT_CALL(m_console, Print("0000: 41 42 43 44 45 46 47 48 -- 49 4a 4b 4c 4d 4e 4f 50   ABCDEFGHIJKLMNOP"));

	m_sut.Process(2, argv);
}