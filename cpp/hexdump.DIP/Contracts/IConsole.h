#pragma once
#include <string>

struct IConsole
{
	virtual void Print(const std::string& message) = 0;
	virtual void PrintError(const std::string& message) = 0;
	virtual ~IConsole() {}
};
