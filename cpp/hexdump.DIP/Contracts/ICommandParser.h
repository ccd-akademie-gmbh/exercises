#pragma once
#include <string>
#include <optional>

struct ICommandParser
{
	virtual std::optional<std::string> ParseForFile(int argc, const char* argv[]) = 0;
	virtual ~ICommandParser() {}
};
