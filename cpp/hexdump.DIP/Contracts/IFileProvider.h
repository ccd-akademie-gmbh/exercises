#pragma once
#include <string>

struct IFileProvider
{
	virtual bool Open(const std::string& filename) = 0;
	// read binary data in buffer until length reached or no more data to read
	// returns number read bytes
	virtual int Read(unsigned char* buffer, int length) = 0;
	virtual ~IFileProvider() {}
};
