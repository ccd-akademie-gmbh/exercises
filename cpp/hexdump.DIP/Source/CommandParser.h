#pragma once
#include "ICommandParser.h"

class CommandParser : public ICommandParser
{
public:
	std::optional<std::string> ParseForFile(int argc, const char* argv[]) override;
};
