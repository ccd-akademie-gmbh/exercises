#include "Hexdump.h"
#include "FileProvider.h"
#include "CommandParser.h"
#include "Console.h"

int main(int argc, const char* argv[])
{
	FileProvider file;
	Console console;
	CommandParser parser;
	Hexdump hexdump(&console, &parser, &file);
	return hexdump.Process(argc, argv);
}
