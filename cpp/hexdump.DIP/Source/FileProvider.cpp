#include "FileProvider.h"

bool FileProvider::Open(const std::string& filename)
{
	m_file.open(filename, std::ios::binary);
	return m_file.is_open();
}

int FileProvider::Read(unsigned char* buffer, int length)
{
	m_file.read(reinterpret_cast<char*>(buffer), length);
	return m_file ? m_file.gcount() : 0;
}
