#pragma once
#include "IConsole.h"
#include "ICommandParser.h"
#include "IFileProvider.h"

class Hexdump
{
public:
	Hexdump(IConsole*, ICommandParser*, IFileProvider*);
	int Process(int argc, const char* argv[]);

private:
	IConsole* m_console;
	ICommandParser* m_command;
	IFileProvider* m_file;
};
