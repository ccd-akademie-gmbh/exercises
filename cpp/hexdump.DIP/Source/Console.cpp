#include "Console.h"
#include <iostream>

void Console::Print(const std::string& message)
{
	std::cout << message << std::endl;
}

void Console::PrintError(const std::string& message)
{
	std::cerr << message << std::endl;
}
