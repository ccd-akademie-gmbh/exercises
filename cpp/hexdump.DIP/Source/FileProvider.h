#pragma once
#include "IFileProvider.h"
#include <fstream>

class FileProvider : public IFileProvider
{
public:
	bool Open(const std::string& filename) override;
	int Read(unsigned char* buffer, int length) override;

private:
	std::ifstream m_file;
};
