#include "Hexdump.h"
#include <sstream>
#include <iomanip>

Hexdump::Hexdump(IConsole* ui, ICommandParser* c, IFileProvider* f)
	: m_console(ui)
	, m_command(c)
	, m_file(f)
{}

int Hexdump::Process(int argc, const char* argv[])
{
	auto filename = m_command->ParseForFile(argc, argv);
	if(!filename)
	{
		m_console->PrintError("Usage: hexdump <filename>");
		return 1;
	}

	if(!m_file->Open(*filename))
	{
		m_console->PrintError("No such file: " + *filename);
		return 2;
	}

	int position = 0;
	unsigned char buffer[16];
	int bytesRead = m_file->Read(buffer, std::size(buffer));
	while(bytesRead > 0)
	{
		std::stringstream out;
		out << std::hex << std::setw(4) << std::setfill('0') << position << ": ";
		for(int i = 0; i < 16; i++)
		{
			if(i < bytesRead)
			{
				out << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(buffer[i]) << " ";
			}
			else
			{
				out << "   ";
			}

			if(i == 7)
			{
				out << "-- ";
			}

			if(buffer[i] < 32 || buffer[i] > 250)
			{
				buffer[i] = '.';
			}
		}
		out << "  " << std::string(reinterpret_cast<char*>(buffer), bytesRead);
		position += bytesRead;
		m_console->Print(out.str());
		bytesRead = m_file->Read(buffer, std::size(buffer));
	}
	return 0;
}
