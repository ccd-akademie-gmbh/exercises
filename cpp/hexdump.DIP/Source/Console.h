#pragma once
#include "IConsole.h"

class Console : public IConsole
{
public:
	void Print(const std::string& message) override;
	void PrintError(const std::string& message) override;
};
