#include "CommandParser.h"

std::optional<std::string> CommandParser::ParseForFile(int argc, const char* argv[])
{
	if(argc != 2)
	{
		return {};
	}
	return argv[1];
}
