#include <iostream>
#include <cstdlib> // atoi

// Print a list of the numbers from 1 to n to the console. 
// For numbers that are multiples of 3 print "Fizz" instead.
// For numbers that are multiples of 5 print "Buzz" instead.
// For numbers that are both multiples of 3 and 5 print "FizzBuzz" instead.

int main(int argc, char* argv[])
{
    int n = 100;
    if(argc == 2)
        n = std::atoi(argv[1]);
    for(int i = 1; i <= n; i++)
    {
        if(i % 3 == 0 && i % 5 == 0)
        {
            std::cout << "FizzBuzz" << std::endl;
        }
        else
        {
            if(i % 3 == 0)
            {
                std::cout << "Fizz"  << std::endl;
            }
            else
            {
                if(i % 5 == 0)
                {
                    std::cout << "Buzz" << std::endl;
                }
                else
                {
                    std::cout << i << std::endl;
                }
            }
        }
    }
}
