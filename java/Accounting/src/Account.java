import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class Account {
    private List<Booking> entries;
    private char delimiter;
    private String filename;
    private String depositName;
    private String balanceName;

    public Account() {
        this.entries = new ArrayList<>();
        this.delimiter = ':';
        this.filename = "Account.txt";
        this.depositName = "deposit";
        this.balanceName = "balance";
    }

    public void run(List<String> args) throws ParseException {
        if (args.isEmpty()) {
            System.out.println("Usage\n"
                    + "out [name] [amount]\n"
                    + "in [amount]\n"
                    + "overview\n\n"
                    + "e.g. Accounting in 250 out 'Bowling' 25.32 out Dinner 64.97 overview");
            return;
        }

        while (!args.isEmpty()) {
            String command = args.removeFirst();
            switch (command) {
                case "in":
                    load();
                    entries.add(Booking.fromArgs(Arrays.asList(depositName, args.removeFirst()), delimiter));
                    save();
                    Booking depositEntry = new Booking(balanceName, total(), new Date());
                    depositEntry.print(System.out);
                    break;

                case "out":
                    load();
                    entries.add(Booking.fromArgs(Arrays.asList(args.removeFirst(), args.removeFirst()), delimiter));
                    save();
                    Booking lastEntry = entries.getLast();
                    lastEntry.print(System.out);
                    Booking balanceEntry = new Booking(balanceName, total(), new Date());
                    balanceEntry.print(System.out);
                    break;

                case "overview":
                    load();
                    print();
                    break;

                default:
                    System.err.println("Unknown command " + command);
                    break;
            }
        }
    }

    public void load() {
        File file = new File(filename);
        if (file.exists()) {
            try (Scanner scanner = new Scanner(file)) {
                entries.clear();
                while (scanner.hasNextLine()) {
                    Booking booking = new Booking();
                    booking.load(scanner.nextLine(), delimiter);
                    entries.add(booking);
                }
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void print() throws ParseException {
        System.out.println("Account entries:");
        for (Booking e : entries) {
            e.print(System.out);
        }
        Booking totalEntry = Booking.fromArgs(Arrays.asList(balanceName, String.valueOf(total())), delimiter);
        totalEntry.print(System.out);
    }

    public void save() {
        try (PrintStream out = new PrintStream(new FileOutputStream(filename, false))) {
            for (Booking e : entries) {
                e.save(out, delimiter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double total() {
        double total = 0.0;
        for (Booking e : entries) {
            if (e.getName().equals(depositName)) {
                total += e.getAmount();
            } else {
                total -= e.getAmount();
            }
        }
        return total;
    }
}
