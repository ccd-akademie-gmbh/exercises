import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws ParseException {
        Account account = new Account();
        account.run(new ArrayList<String>(Arrays.asList(args)));
    }
}
