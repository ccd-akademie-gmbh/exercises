import java.io.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Booking {
    private String name;
    private double amount;
    private Date time;

    public Booking(String name, double amount, Date time) {
        this.name = name;
        this.amount = amount;
        this.time = time;
    }

    public Booking() {
    }

    public static Booking fromArgs(List<String> args, char delim) throws ParseException {
        Date now = new Date();
        String name = args.get(0).replace(delim, ';');
        NumberFormat format = NumberFormat.getInstance(Locale.getDefault());
        double amount = format.parse(args.get(1)).doubleValue();
        return new Booking(name, amount, now);
    }

    public void print(PrintStream out) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        out.printf("%-30s %-20s = %10.2f%n", dateFormat.format(time), name, amount);
    }

    public void load(String line, char delim) throws ParseException {
        String[] parts = line.split(String.valueOf(delim));
        this.name = parts[0];
        NumberFormat format = NumberFormat.getInstance(Locale.getDefault());
        this.amount = format.parse(parts[1]).doubleValue();
        this.time = new Date(Long.parseLong(parts[2]));
    }

    public void save(PrintStream out, char delim) {
        out.printf("%s%c%.2f%c%d%n", name, delim, amount, delim, time.getTime());
    }

    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }

    public Date getTime() {
        return time;
    }
}
