
public class Templates {
    public String get(int frequency) {
        switch (frequency) {
            case 1:
                return "Hello, ";
            case 2:
                return "Welcome back, ";
            default:
                return "Hello my good friend, ";
        }
    }
}
