import java.util.Map;

public class BusinessLogic {
    public String determineGreeting(String name, Map<String, Integer> visits) {
        Templates templates = new Templates();
        if (visits.containsKey(name)) {
            int visitCount = visits.get(name);
            String result = templates.get(visitCount);
            return result + name + "!";
        }
        return "Hello " + name + "!";
    }
}
