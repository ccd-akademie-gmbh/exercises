import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        BusinessLogic logic = new BusinessLogic();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Name: ");
            String name = scanner.nextLine();
            try {
                FileHelper.appendAllLines("guestlist.txt", Collections.singletonList(name));
                List<String> names = FileHelper.readAllLines("guestlist.txt");

                Map<String, Integer> visits = new HashMap<>();
                for (String n : names) {
                    visits.put(n, visits.getOrDefault(n, 0) + 1);
                }

                String greeting = logic.determineGreeting(name, visits);
                System.out.println(greeting);
            } catch (IOException e) {
                System.err.println("Error while reading or writing the file: " + e.getMessage());
            }
        }
    }
}
