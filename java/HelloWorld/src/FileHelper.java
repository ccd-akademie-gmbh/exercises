import java.io.*;
import java.nio.file.*;
import java.util.*;

public class FileHelper {
    public static void appendAllLines(String filePath, List<String> lines) throws IOException {
        Files.write(Paths.get(filePath), lines, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
    }

    public static List<String> readAllLines(String filePath) throws IOException {
        return Files.readAllLines(Paths.get(filePath));
    }
}
