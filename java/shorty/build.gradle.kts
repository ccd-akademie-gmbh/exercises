plugins {
    id("java")
    id("info.solidsoft.pitest") version "1.15.0"
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.10.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

configure<info.solidsoft.gradle.pitest.PitestPluginExtension> {
    junit5PluginVersion.set("1.2.1")
    pitestVersion.set("1.15.2")
    avoidCallsTo.set(setOf("java.lang"))
    mutators.set(setOf("STRONGER"))
    targetClasses.set(setOf("ccd.*")) // Anpassen an Ihr Paket
    targetTests.set(setOf("ccd.*")) // Anpassen an Ihr Testpaket
    threads.set(Runtime.getRuntime().availableProcessors())
    outputFormats.set(setOf("XML", "HTML"))
    mutationThreshold.set(75)
    coverageThreshold.set(60)
}

tasks.named("check") {
    dependsOn(":pitest")
}