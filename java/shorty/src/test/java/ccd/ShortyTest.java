package ccd;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ShortyTest {

    @Test
    void shorterStringIsReturnedAsIs() {
        assertEquals("A", Shorty.shorten("A", 5));
    }
}