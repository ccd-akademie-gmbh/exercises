package ccd;

public class Shorty {
    public static String shorten(String s, int length) {
        if (s.length() <= length) {
            return s;
        }

        if (length < 7) {
            return s.substring(0, length);
        }

        int sampleLength = (length - 3) / 2;
        int extraLength = 0;
        if (sampleLength * 2 + 3 < length) {
            extraLength = 1;
        }

        String prefix = s.substring(0, sampleLength + extraLength);
        String suffix = s.substring(s.length() - sampleLength);
        return prefix + "..." + suffix;
    }
}