package de.ccd;

import java.time.LocalDate;

public class OneTimeTask extends Task {
    public OneTimeTask(String title, String description, String status, int priority, LocalDate deadline) {
        super(title, description, status, priority, deadline);
    }

    @Override
    public void setRecurringInterval(int days) {
        throw new UnsupportedOperationException("One-time tasks cannot have a recurring interval.");
    }

    @Override
    public void assignOwner(String owner) {
        // Ignore: One-time tasks do not have an owner
    }

    @Override
    public boolean isOverdue() {
        return super.getDeadline().isBefore(LocalDate.now());
    }
}
