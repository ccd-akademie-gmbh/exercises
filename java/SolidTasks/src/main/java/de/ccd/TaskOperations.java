package de.ccd;

import java.time.LocalDate;

public interface TaskOperations {
    void displayTaskDetails();

    void setRecurringInterval(int days);

    void assignOwner(String owner);

    void setDeadline(LocalDate deadline);

    boolean isOverdue();
}

