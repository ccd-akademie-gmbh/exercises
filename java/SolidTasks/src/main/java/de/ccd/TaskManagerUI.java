package de.ccd;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class TaskManagerUI {
    private final TaskManager taskManager;
    private final Scanner scanner;

    public TaskManagerUI() {
        this.taskManager = new TaskManager();
        this.scanner = new Scanner(System.in);
    }

    public void start() {
        loadTasks();

        boolean running = true;
        while (running) {
            System.out.println("\n=== Task Manager ===");
            System.out.println("1. Add Task");
            System.out.println("2. Remove Task");
            System.out.println("3. List All Tasks");
            System.out.println("4. Sort Tasks");
            System.out.println("5. Filter Tasks");
            System.out.println("6. Save Tasks");
            System.out.println("7. Exit");
            System.out.print("Select an option: ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            switch (choice) {
                case 1 -> addTask();
                case 2 -> removeTask();
                case 3 -> listTasks();
                case 4 -> sortTasks();
                case 5 -> filterTasks();
                case 6 -> saveTasks();
                case 7 -> {
                    running = false;
                    System.out.println("Exiting Task Manager. Goodbye!");
                }
                default -> System.out.println("Invalid choice. Please try again.");
            }
        }
    }


    private void addTask() {
        System.out.println("\nSelect Task Type:");
        System.out.println("1. One time Task");
        System.out.println("2. Recurring Task");
        System.out.println("3. Important Task");
        System.out.print("Choose an option: ");
        int taskType = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        System.out.print("Enter task title: ");
        String title = scanner.nextLine();

        System.out.print("Enter task description: ");
        String description = scanner.nextLine();

        System.out.print("Enter task status (NEW, IN_PROGRESS, DONE): ");
        String status = scanner.nextLine();

        System.out.print("Enter task priority (1-5): ");
        int priority = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        System.out.print("Enter task deadline (yyyy-mm-dd): ");
        String deadlineInput = scanner.nextLine();
        LocalDate deadline = LocalDate.parse(deadlineInput);

        Task task;

        switch (taskType) {
            case 1 -> {
                task = new OneTimeTask(title, description, status, priority, deadline);
                System.out.println("General Task created successfully!");
            }
            case 2 -> {
                System.out.print("Enter recurrence interval in days: ");
                int recurrenceInterval = scanner.nextInt();
                scanner.nextLine(); // Consume newline
                task = new RecurringTask(title, description, status, priority, deadline, recurrenceInterval);
                System.out.println("Recurring Task created successfully!");
            }
            case 3 -> {
                System.out.print("Enter owner name: ");
                String owner = scanner.nextLine();
                task = new ImportantTask(title, description, status, priority, deadline, owner);
                System.out.println("Important Task created successfully!");
            }
            default -> {
                System.out.println("Invalid task type. Task creation canceled.");
                return;
            }
        }

        taskManager.addTask(task);
    }

    private void removeTask() {
        System.out.print("Enter the title of the task to remove: ");
        String titleToRemove = scanner.nextLine();

        // Suche nach der Task mit dem angegebenen Titel
        Task taskToRemove = null;
        for (Task task : taskManager.getTasks()) {
            if (task.getTitle().equalsIgnoreCase(titleToRemove)) {
                taskToRemove = task;
                break;
            }
        }

        if (taskToRemove != null) {
            taskManager.removeTask(taskToRemove);
            System.out.println("Task removed successfully.");
        } else {
            System.out.println("Task not found.");
        }
    }

    private void listTasks() {
        System.out.println("\n=== All Tasks ===");
        taskManager.listAllTasks();
    }

    private void sortTasks() {
        System.out.println("\nSort by:");
        System.out.println("1. Priority");
        System.out.println("2. Status");
        System.out.println("3. Title");
        System.out.print("Choose an option: ");
        int choice = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        SortCriteria criteria;
        switch (choice) {
            case 1 -> criteria = SortCriteria.PRIORITY;
            case 2 -> criteria = SortCriteria.STATUS;
            case 3 -> criteria = SortCriteria.TITLE;
            default -> {
                System.out.println("Invalid choice. Returning to main menu.");
                return;
            }
        }

        taskManager.sortBy(criteria);
        System.out.println("Tasks sorted successfully.");
    }

    private void filterTasks() {
        System.out.println("\nFilter by:");
        System.out.println("1. Status");
        System.out.println("2. Priority Range");
        System.out.print("Choose an option: ");
        int choice = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        switch (choice) {
            case 1 -> {
                System.out.print("Enter status (NEW, IN_PROGRESS, DONE): ");
                String status = scanner.nextLine();
                List<Task> filteredByStatus = taskManager.filterBy(FilterCriteria.STATUS, status);
                System.out.println("\nFiltered Tasks:");
                filteredByStatus.forEach(Task::displayTaskDetails);
            }
            case 2 -> {
                System.out.print("Enter minimum priority: ");
                int minPriority = scanner.nextInt();
                System.out.print("Enter maximum priority: ");
                int maxPriority = scanner.nextInt();
                scanner.nextLine(); // Consume newline
                List<Task> filteredByPriority = taskManager.filterBy(FilterCriteria.PRIORITY_RANGE, minPriority, maxPriority);
                System.out.println("\nFiltered Tasks:");
                filteredByPriority.forEach(Task::displayTaskDetails);
            }
            default -> System.out.println("Invalid choice. Returning to main menu.");
        }
    }

    private void saveTasks() {
        try {
            taskManager.saveTasks();
            System.out.println("Tasks saved successfully!");
        } catch (IOException e) {
            System.out.println("Error saving tasks: " + e.getMessage());
        }
    }

    private void loadTasks() {
        try {
            taskManager.loadTasks();
            System.out.println("Tasks loaded successfully!");
        } catch (IOException e) {
            System.out.println("Error loading tasks: " + e.getMessage());
        }
    }
}

