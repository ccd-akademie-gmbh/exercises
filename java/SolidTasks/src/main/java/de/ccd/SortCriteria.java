package de.ccd;

public enum SortCriteria {
    PRIORITY,
    STATUS,
    TITLE
}
