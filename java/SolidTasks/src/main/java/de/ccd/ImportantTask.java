package de.ccd;

import java.time.LocalDate;

public class ImportantTask extends Task {
    private String owner;

    public ImportantTask(String title, String description, String status, int priority, LocalDate deadline, String owner) {
        super(title, description, status, priority, deadline);
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public void assignOwner(String owner) {
        this.owner = owner;
        System.out.println("Owner assigned to: " + owner);
    }

    @Override
    public void setRecurringInterval(int days) {
        throw new UnsupportedOperationException("Important tasks cannot have a recurring interval.");
    }

    @Override
    public void setDeadline(LocalDate deadline) {
        if (deadline.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Important tasks cannot have deadlines in the past!");
        }
        super.setDeadline(deadline);
    }
}
