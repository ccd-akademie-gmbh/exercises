package de.ccd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskManager {
    private List<Task> tasks = new ArrayList<>();
    private final TaskPersistence persistence = new TaskPersistence();

    public List<Task> getTasks() {
        return tasks;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void removeTask(TaskOperations task) {
        tasks.remove(task);
    }

    public List<Task> filterBy(FilterCriteria criteria, Object... args) {
        List<Task> filteredTasks = new ArrayList<>();

        if (criteria == FilterCriteria.STATUS) {
            String status = (String) args[0];
            for (Task task : tasks) {
                if (task.getStatus().equalsIgnoreCase(status)) {
                    filteredTasks.add(task);
                }
            }
        } else if (criteria == FilterCriteria.PRIORITY_RANGE) {
            int minPriority = (int) args[0];
            int maxPriority = (int) args[1];
            for (Task task : tasks) {
                if (task.getPriority() >= minPriority && task.getPriority() <= maxPriority) {
                    filteredTasks.add(task);
                }
            }
        } else {
            System.out.println("Invalid filter criteria.");
        }

        return filteredTasks;
    }

    public void sortBy(SortCriteria criteria) {
        if (criteria == SortCriteria.PRIORITY) {
            tasks.sort(Comparator.comparingInt(Task::getPriority));
        } else if (criteria == SortCriteria.STATUS) {
            tasks.sort((t1, t2) -> t1.getStatus().compareToIgnoreCase(t2.getStatus()));
        } else if (criteria == SortCriteria.TITLE) {
            tasks.sort((t1, t2) -> t1.getTitle().compareToIgnoreCase(t2.getTitle()));
        } else {
            System.out.println("Invalid sort criteria.");
        }
    }

    public void saveTasks() throws IOException {
        persistence.saveTasks(tasks);
    }

    public void loadTasks() throws IOException {
        tasks = persistence.loadTasks();
    }

    public void assignOwnerToTask(TaskOperations task, String owner) {
        task.assignOwner(owner);
    }

    public void setRecurringIntervalForTask(TaskOperations task, int days) {
        task.setRecurringInterval(days);
    }

    public void listAllTasks() {
        for (TaskOperations task : tasks) {
            task.displayTaskDetails();
        }
    }
}

