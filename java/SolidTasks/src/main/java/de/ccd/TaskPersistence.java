package de.ccd;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TaskPersistence {
    private static final String FILE_NAME = "tasks.txt";

    public void saveTasks(List<Task> tasks) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME))) {
            for (Task task : tasks) {
                if (task instanceof RecurringTask) {
                    RecurringTask recurringTask = (RecurringTask) task;
                    writer.write("RECURRING;" + recurringTask.getTitle() + ";" + recurringTask.getDescription() + ";" +
                            recurringTask.getStatus() + ";" + recurringTask.getPriority() + ";" + recurringTask.getDeadline() + ";" +
                            recurringTask.getRecurrenceIntervalDays() + "\n");
                } else if (task instanceof ImportantTask) {
                    ImportantTask importantTask = (ImportantTask) task;
                    writer.write("IMPORTANT;" + importantTask.getTitle() + ";" + importantTask.getDescription() + ";" +
                            importantTask.getStatus() + ";" + importantTask.getPriority() + ";" + importantTask.getDeadline() + ";" +
                            importantTask.getOwner() + "\n");
                } else if (task instanceof OneTimeTask) {
                    writer.write("ONETIME;" + task.getTitle() + ";" + task.getDescription() + ";" +
                            task.getStatus() + ";" + task.getPriority() + ";" + task.getDeadline() + "\n");
                }
            }
        }
    }

    public List<Task> loadTasks() throws IOException {
        List<Task> tasks = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(";");
                String type = parts[0];
                String title = parts[1];
                String description = parts[2];
                String status = parts[3];
                int priority = Integer.parseInt(parts[4]);
                LocalDate deadline = LocalDate.parse(parts[5]);

                Task task;
                switch (type) {
                    case "RECURRING" -> {
                        int recurrenceIntervalDays = Integer.parseInt(parts[6]);
                        task = new RecurringTask(title, description, status, priority, deadline, recurrenceIntervalDays);
                    }
                    case "IMPORTANT" -> {
                        String owner = parts[6];
                        task = new ImportantTask(title, description, status, priority, deadline, owner);
                    }
                    case "ONETIME" -> {
                        task = new OneTimeTask(title, description, status, priority, deadline);
                    }
                    default -> throw new IllegalArgumentException("Unknown task type: " + type);
                }
                tasks.add(task);
            }
        }
        return tasks;
    }
}
