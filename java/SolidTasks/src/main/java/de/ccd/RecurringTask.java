package de.ccd;

import java.time.LocalDate;

public class RecurringTask extends Task {
    private int recurrenceIntervalDays; // Interval in days

    public RecurringTask(String title, String description, String status, int priority, LocalDate deadline, int recurrenceIntervalDays) {
        super(title, description, status, priority, deadline);
        this.recurrenceIntervalDays = recurrenceIntervalDays;
    }

    public int getRecurrenceIntervalDays() {
        return recurrenceIntervalDays;
    }

    public void setRecurrenceIntervalDays(int days) {
        this.recurrenceIntervalDays = days;
    }

    @Override
    public void setRecurringInterval(int days) {
        this.recurrenceIntervalDays = days;
        System.out.println("Recurring interval set to: " + days + " days");
    }

    @Override
    public void assignOwner(String owner) {
        // Ignore: Recurring tasks do not have an owner
    }

    @Override
    public boolean isOverdue() {
        return super.getDeadline().isBefore(LocalDate.now());
    }
}
