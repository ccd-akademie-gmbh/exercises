package de.ccd;
import java.time.LocalDate;

public abstract class Task implements TaskOperations{
    private final String title;
    private final String description;
    private final String status; // e.g., "NEW", "IN_PROGRESS", "DONE"
    private final int priority;
    private LocalDate deadline;

    public Task(String title, String description, String status, int priority, LocalDate deadline) {
        this.title = title;
        this.description = description;
        this.status = status;
        this.priority = priority;
        this.deadline = deadline;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    public int getPriority() {
        return priority;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
        System.out.println("Deadline set to: " + deadline);
    }

    public boolean isOverdue() {
        return deadline.isBefore(LocalDate.now());
    }

    public void displayTaskDetails() {
        System.out.println("Title: " + title);
        System.out.println("Description: " + description);
        System.out.println("Status: " + status);
        System.out.println("Priority: " + priority);
        System.out.println("Deadline: " + deadline);
    }
}